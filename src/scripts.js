$(document).ready(function() {
    maxId = 0;
    let color = "#2196F3";
    colorBorder();

    // Render bottom border for input field
    function colorBorder() {
        $("input[type=text]").css("border-bottom", "2px solid" + color + "");
    }

    // Define card
    function card(color, text) {
        maxId++;
        return (
            `<div class="kanban-card d-flex flex-column" id = "`+maxId+ `" style="background-color:` +
            color +
            `">
            <p>` +
                text +
                `</p>
            <div class="buttons">
             <button class = "read">
               <span class="oi oi-pencil"></span>
             </button>
             <button class="delete">
               <span class="oi oi-x"></span>
             </button>
             </div>
        </div>`
        );
    }

    // Add new card
    $("input[type=text]").keypress(function(event) {
        if (event.which === 13) {
            let text = $(this).val();
            $(this).val("");
            $(this)
                .next(".list-body")
                .append(card(color, text));
        }
    });

    //Refactor Card
    $(document).on("click", ".kanban-card > div.buttons > button.read", function() {

        let text = $("input[type=text]").val();
        $("input[type=text]").val("");
        $(this).parent().parent().html(`<p>` +
                text +
                `</p>
        <div class="buttons">
            <button class = "read">
            <span class="oi oi-pencil"></span>
            </button>
            <button class="delete">
            <span class="oi oi-x"></span>
            </button>
            </div>
            </div>`)

    });
    // Удалить карточку
    $(document).on("click", ".kanban-card > div.buttons > button.delete", function() {
        $(this)
            .parent()
            .parent()
            .fadeOut(250, function() {
                $(this).remove();
            });
    });


    // Показать/скрыть поле для ввода
    $("header").on("click", "button", function() {
        $(this)
            .children()
            .toggleClass("oi-chevron-top oi-chevron-bottom");
        $("input[type=text]").slideToggle();
    });


    $(".color").on("click", function() {
        color = $(this).val();
        $(".oi-check").removeClass("oi-check");
        colorBorder();
        $(this)
            .children()
            .addClass("oi-check");
    });

    $(function() {
        $("#sortable1, #sortable2, #sortable3")
            .sortable({
                connectWith: ".list-body"
            })
            .disableSelection();
    });
});
